// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function filterBMWAndAudi(inventory) {
    try {
        
        let BMWAndAudi = [];

        // Iterate through each car in the inventory
        for (let i = 0; i < inventory.length; i++) {
            // Check if the car's make is BMW or Audi
            if (inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi') {
                // If the condition is true, push the car to the BMWAndAudi array
                BMWAndAudi.push(inventory[i]);
            }
        }

        // Return the array of BMW and Audi cars
        return BMWAndAudi;
    } catch (error) {
        console.error('Error in filterBMWAndAudi function:', error.message);
        return [];
    }
}





module.exports = filterBMWAndAudi;
