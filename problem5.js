// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function findOlderCars(inventory, year) {
    try {
        // Check if year is provided and is a number
        if (typeof year !== 'number' || isNaN(year)) {
            throw new Error('Year should be a valid number');
        }

        // Initialize an empty array to store older cars
        let olderCars = [];
        
        // Iterate through each car in the inventory
        for (let i = 0; i < inventory.length; i++) {
            // Check if the car's year is less than the specified year
            if (inventory[i].car_year < year) {
                // If yes, push the car to the olderCars array
                olderCars.push(inventory[i]);
            }
        }
        
        // Return the array of older cars
        return olderCars;
    } catch (error) {
        console.error('Error in findOlderCars function:', error.message);
        return [];
    }
}

module.exports = findOlderCars;
