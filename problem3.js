// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModelsAlphabetically(inventory) {
    try {
      
        let carModels = [];
        for (let i = 0; i < inventory.length; i++) {
            carModels.push(inventory[i].car_model);
        }

        // Sort car models alphabetically using bubble sort
        for (let i = 0; i < carModels.length - 1; i++) {
            for (let j = 0; j < carModels.length - i - 1; j++) {
                if (carModels[j] > carModels[j + 1]) {
                    // Swap car models
                    let temp = carModels[j];
                    carModels[j] = carModels[j + 1];
                    carModels[j + 1] = temp;
                }
            }
        }

        return carModels;
    } catch (error) {
        console.error('Error in sortCarModelsAlphabetically function:', error.message);
        return [];
    }
}

module.exports = sortCarModelsAlphabetically;

